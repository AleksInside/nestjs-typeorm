import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Put,
  Request,
  UseGuards,
} from '@nestjs/common';
import { DogDto } from './interfaces/dog.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { DogRepository } from './dogs.repository';
import { JwtAuthGuard } from '../auth/jwt.guard';

@Controller('dogs')
export class DogsController {
  constructor(
    @InjectRepository(DogRepository)
    private readonly dogRepository: DogRepository,
  ) {}

  @Post()
  create(@Body() dogDto: DogDto) {
    return this.dogRepository.createDog(dogDto);
  }

  @Get()
  findAll() {
    return this.dogRepository.find();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.dogRepository.findOneDog(id);
  }

  @Put(':id')
  update(@Param('id') id: string, @Body() dogDto: DogDto) {
    return this.dogRepository.updateDog(id, dogDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.dogRepository.removeDog(id);
  }

  @UseGuards(JwtAuthGuard)
  @Post('oldDogs')
  getOlderDog(@Request() req) {
    return this.dogRepository.findOlderDogs(req.body.age);
  }
}
