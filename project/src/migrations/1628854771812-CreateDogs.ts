import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateDog1553858330734 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`CREATE TABLE "dog"
                             (
                                 "id"    SERIAL                NOT NULL,
                                 "name"  character varying(50) NOT NULL,
                                 "age"   integer               NOT NULL,
                                 "breed" character varying(100),
                                 CONSTRAINT "PK_c0911b1d44db6cdd303c6d6afc9" PRIMARY KEY ("id")
                             )`);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP TABLE "dog"`);
  }
}
