import { Injectable } from '@nestjs/common';
import { AdminUserEntity } from 'nestjs-admin';

@Injectable()
export class UsersService {
  private readonly users: Array<AdminUserEntity> = [
    {
      id: '1',
      username: 'john',
      password: 'changeme',
    },
    {
      id: '2',
      username: 'maria',
      password: 'guess',
    },
  ];

  async findOne(username: string): Promise<AdminUserEntity | undefined> {
    console.log('da');
    return this.users.find((user) => user.username === username);
  }
}
