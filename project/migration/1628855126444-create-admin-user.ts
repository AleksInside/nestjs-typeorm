import {MigrationInterface, QueryRunner} from "typeorm";

export class createAdminUser1628855126444 implements MigrationInterface {
    name = 'createAdminUser1628855126444'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE \`dogs\`.\`dog\` (\`id\` int NOT NULL AUTO_INCREMENT, \`name\` varchar(50) NOT NULL, \`age\` int NOT NULL, \`breed\` varchar(100) NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`dogs\`.\`adminUser\` (\`id\` char(36) NOT NULL, \`username\` varchar(50) NOT NULL, \`password\` varchar(128) NOT NULL, UNIQUE INDEX \`IDX_58bd2b086488ba1ba90847a192\` (\`username\`), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX \`IDX_58bd2b086488ba1ba90847a192\` ON \`dogs\`.\`adminUser\``);
        await queryRunner.query(`DROP TABLE \`dogs\`.\`adminUser\``);
        await queryRunner.query(`DROP TABLE \`dogs\`.\`dog\``);
    }

}
